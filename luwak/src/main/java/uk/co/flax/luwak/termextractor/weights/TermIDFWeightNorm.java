package uk.co.flax.luwak.termextractor.weights;

import java.util.Map;
import uk.co.flax.luwak.termextractor.QueryTerm;
/*
 * Copyright (c) 2014 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Weights more infrequent terms more highly
 */
public class TermIDFWeightNorm extends WeightNorm {

    private final Map<String, Float> frequencies;

    /**
     * Creates a TermFrequencyNorm
     * @param frequencies map of terms to term frequencies
     */
    public TermIDFWeightNorm(Map<String, Float> frequencies) {
        this.frequencies = frequencies;
    }

    @Override
    public float norm(QueryTerm term) {
        Float mapVal = this.frequencies.get(term.term.text());
        if (mapVal != null)
            return mapVal;
        return 14; //or return a large val
//        return (float)1.00001;
//        return (float)1.1;
    }

}
