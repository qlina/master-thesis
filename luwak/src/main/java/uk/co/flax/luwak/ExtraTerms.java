package uk.co.flax.luwak;

import org.apache.lucene.store.InputStreamDataInput;
import org.apache.lucene.store.OutputStreamDataOutput;
import org.apache.lucene.util.BytesRef;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ExtraTerms {
//    private final List<BytesRef> termlist;

//    public ExtraTerms(List<BytesRef> termlist) {
//        this.termlist = termlist;
//    }

    public static List<String> deserialize(BytesRef bytes, int size)  {

        ByteArrayInputStream is = new ByteArrayInputStream(bytes.bytes);
        try (InputStreamDataInput data = new InputStreamDataInput(is)) {
            List<String> strList = new ArrayList<>();
            for(int i = 0; i < size; i++) {
                strList.add(data.readString());
            }
            return strList;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static BytesRef serialize(List<BytesRef> termlist) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try (OutputStreamDataOutput data = new OutputStreamDataOutput(os)) {
            for (BytesRef ref : termlist) {
                data.writeString(ref.utf8ToString());
            }
            return new BytesRef(os.toByteArray());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
