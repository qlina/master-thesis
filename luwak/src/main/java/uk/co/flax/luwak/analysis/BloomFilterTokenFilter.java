//package uk.co.flax.luwak.analysis;
//
//import java.io.IOException;
//
//import com.google.common.hash.BloomFilter;
//import org.apache.lucene.analysis.TokenStream;
//import org.apache.lucene.analysis.tokenattributes.TermToBytesRefAttribute;
//import org.apache.lucene.analysis.FilteringTokenFilter;
//import org.apache.lucene.util.BytesRefHash;
//import uk.co.flax.luwak.main;
//
//public class BloomFilterTokenFilter extends FilteringTokenFilter{
//
//    private final BloomFilter bloomFilter = main.bloomFilter;
//    private final TermToBytesRefAttribute bytesAtt = addAttribute(TermToBytesRefAttribute.class);
//
//    public BloomFilterTokenFilter(TokenStream ts) {
//        super(ts);
//    }
//
//    @Override
//    protected boolean accept() throws IOException {
////        if(bytesAtt.getBytesRef().utf8ToString().equalsIgnoreCase("com"))
////            System.out.println("com");
//        return bloomFilter.mightContain(bytesAtt.getBytesRef().utf8ToString());
//    }
//
//}
