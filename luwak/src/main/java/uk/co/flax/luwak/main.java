package uk.co.flax.luwak;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.*;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import uk.co.flax.luwak.analysis.TermsEnumTokenStream;
import uk.co.flax.luwak.matchers.SimpleMatcher;
import uk.co.flax.luwak.presearcher.TermFilteredPresearcher;
import uk.co.flax.luwak.queryparsers.LuceneQueryParser;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import uk.co.flax.luwak.util.MadePair;
import uk.co.flax.luwak.util.WordCooccurrenceMatrix;

public class main
{
    public static final Analyzer ANALYZER = new StandardAnalyzer();
    public static Locale locale = new Locale.Builder().setLanguage("en").setRegion("US").build();
    public static final String FIELD = "text";
    public static int iteration = 0;
    public static int Batch_Size = 1200;
    public static int last_batch_end = 0;
    public static final int Max_Iter = 25;
    // useful things...
    public static long totalTime = 0;
    public static int numExtra = 0;
    public static int wasted_extra = 0;
    public static float numDuplications = 0;
    public static long time_duplications = 0;
    public static long time_check = 0;
    public static float numCheck = 0;
    public static long overhead = 0;
    public static int overheadCount = 0;
    public static int numIntersection = 0;
    public static long avoidSearchTime = 0;
    public static int numAvoid = 0;
    public static int numZeroExtra = 0;
    public static int numFilteredByExtra = 0;
    public static int numDQueryRun = 0;
    public static int totalSearchTime = 0;
    public static int numSingles = 0;
    public static int num2 = 0;
//    public static int allIndexedQ = 0;
//    public static int numOOVs = 0;
//    public static int xth = 0;
    private static final long MEGABYTE = 1024L * 1024L;
    public static long totalMemory = 0L;
//    public static int iniCount = 0;
//    public static Funnel<String> memberFunnel = new Funnel<String>() {
//        public void funnel(String memberId, PrimitiveSink sink) {
//            sink.putString(memberId, Charsets.UTF_8);
//        }
//    };
//    public static BloomFilter bloomFilter;
//    public static SetMultimap<String, Integer> multimap;
    public static Map<String, BitSet> multimap;
//    public static Map<String, String> commonCombinations = readCommonTermsCombination();
//    public static WordCooccurrenceMatrix wordMatrix = new WordCooccurrenceMatrix();
    public static final Logger logger = Logger.getLogger(main.class.getName());
    public static Set<String> falsePositives = new HashSet<>();
    public static int falsePositiveCount = 0;
//    public static Path path = Paths.get("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/compare_results","modified.txt");
    public static Path path = Paths.get("/home/lina/luwak-nov18-Feb11_space/luwak","feb11.txt");

    public static void main(String... args) throws Exception {
//        configuration.setLanguageModelPath("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/2007_tokenized/model.lm");
//        simpleNGramModel = new SimpleNGramModel("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/2007_tokenized/model.lm",
//                textDictionary, 1, -1);

//        new TrecMain("../src/main/resources/demoqueries", "../src/main/resources/demodocs");
//        new main("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/metadata_test_indexing",
//                "/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/demodocs");
//        new main("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/aol-million-four-terms.txt",
//                "/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/100k/test");
//        new main("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/07_10000_processed.txt",
//                "/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/100k/test");
        new main("/home/lina/aol/AOL-user-ct-collection/aol-million-no-one-term.txt",
                "/home/lina/100k_test");
    }
    public main(String queriesFile, String inputDirectory) throws Exception {
//        Presearcher presearcher = new MultipassTermFilteredPresearcher(PASSES, 0.0f);
        Presearcher presearcher = new TermFilteredPresearcher();
        // get probability
        IndexWriterConfig iwc = new IndexWriterConfig(ANALYZER);
        Directory dir = new RAMDirectory();
        IndexWriter writer = new IndexWriter(dir, iwc);
        try (Monitor monitor = new Monitor(new LuceneQueryParser(FIELD, ANALYZER), presearcher, writer)) {
            addQueries(monitor, queriesFile);
//            Runtime runtime = Runtime.getRuntime();
//            runtime.gc();
//            long memoryUsed = runtime.totalMemory() - runtime.freeMemory();
//            IndexReader reader = DirectoryReader.open(writer, false, false);
            logger.info(String.format(locale,"Documents loaded from %s", inputDirectory));
            for (int iter_id = 0; iter_id < Max_Iter; iter_id++) {
                DocumentBatch batch = DocumentBatch.of(buildDirectoryDocs(Paths.get(inputDirectory).toFile()));
                // adjust * based on the batch size
//                int expectedInsertions = batch.getBatchSize() * 500;
//                bloomFilter = buildBloomFilterMultiMap(batch.getIndexReader(), expectedInsertions);
                long startTime = System.nanoTime();
                buildMultiMap(batch.getIndexReader());
                long usedTime = TimeUnit.MILLISECONDS.convert(System.nanoTime() - startTime, TimeUnit.NANOSECONDS);
//                Runtime sndruntime = Runtime.getRuntime();
//                sndruntime.gc();
//                long memoryAfterMultiMap = sndruntime.totalMemory() - sndruntime.freeMemory();
//                System.out.println("used memory: " + bytesToMegabytes(memoryAfterMultiMap));
//            long usedTime = TimeUnit.MILLISECONDS.convert(System.nanoTime() - startTime, TimeUnit.NANOSECONDS);
//                System.out.println(expectedInsertions);
//                System.out.println(bloomFilter.approximateElementCount());
                // Timer
//                numDuplications = 0;
//                numMatches = 0;
//                time_duplications = 0;
//                time_check = 0;
//                numCheck = 0;
                wasted_extra = 0;
                numFilteredByExtra = 0;
                Matches<QueryMatch> matches = monitor.match(batch, SimpleMatcher.FACTORY);
                outputMatches(matches);
                Runtime runtime = Runtime.getRuntime();
                runtime.gc();
                long memoryUsed = runtime.totalMemory() - runtime.freeMemory();
                totalMemory += bytesToMegabytes(memoryUsed);
                System.out.print("\n");
//                System.out.println("used memory: " + bytesToMegabytes(memoryUsed));
                System.out.println("build bit map time: " + usedTime);
//                System.out.println("num of distinct terms in the batch: " + multimap.keySet().size());
//                System.out.println("num filtered by extra: " + numFilteredByExtra);
//                System.out.println("num of indexed but not used extra:" + wasted_extra);
                System.out.print("\n");
//                totalMemory += bytesToMegabytes(memoryUsed);
//                System.out.println("time check: " + (TimeUnit.MILLISECONDS.convert(time_check, TimeUnit.NANOSECONDS)));
//                System.out.println("time duplication: " + (TimeUnit.MILLISECONDS.convert(time_duplications, TimeUnit.NANOSECONDS)));
//                System.out.println("num check: " + numCheck);
//                System.out.println("avg time duplication: " + (time_duplications/numDuplications));
//                System.out.println("avg time check: " + (time_check/numCheck));
//                System.out.println("avg matches per duplication:" + (numMatches/numDuplications));
            }
//            System.out.println("avg num of postings accessed (candidates selection)" + iniCount/Max_Iter);
//            System.out.println("total time check: " + TimeUnit.MILLISECONDS.convert(time_check, TimeUnit.NANOSECONDS));
//            System.out.println("total time intersection: " + TimeUnit.MILLISECONDS.convert(totalTime, TimeUnit.NANOSECONDS));
//            System.out.println("total time overhead: " + TimeUnit.MILLISECONDS.convert(overhead, TimeUnit.NANOSECONDS) + "\n");
//            System.out.println("num avoid: " + numAvoid);
            System.out.println("num check: " + numCheck);
//            System.out.println("num intersection: " + numIntersection);
//            System.out.println("num overheads: " + overheadCount + "\n");
//            System.out.println("avg time avoid:" + (avoidSearchTime/numAvoid));
//            System.out.println("avg time check: " + (time_check/numCheck));
//            System.out.println("avg time duplication: " + (time_duplications/numDuplications));
//            System.out.println("avg intersection time (ns): " + (totalTime/numIntersection));
//            System.out.println("avg batch overhead (ns): " + overhead/overheadCount + "\n");
//            System.out.println("overheadCount: " + overheadCount);
            System.out.println("num of extra terms: " + numExtra);
            System.out.println(num2 + " " + numSingles);
//            System.out.println("num x th:" + xth);
//            System.out.println("num zero extra: " + numZeroExtra);
            System.out.println("num D Query Run: " + numDQueryRun);
//            System.out.println("num of all indexed queries: " + allIndexedQ);
//            System.out.println("num of OOVs: " + numOOVs);
//            System.out.println("total intersection time: " + (totalTime/1000000));
            System.out.println("total search time: " + totalSearchTime);
//            System.out.println("avg memory per batch in MB: " + bytesToMegabytes(memoryUsed));
            System.out.println("avg memory per batch in MB: " + totalMemory/Max_Iter);

//            long startTime = System.nanoTime();
//            System.out.println("build bf time");
//            System.out.println(usedTime);

        }

    }
    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

//    public static Map<String, String> readCommonTermsCombination() {
//        Map<String, String> combination = new HashMap<>();
////        try (BufferedReader reader = new BufferedReader(new FileReader("/home/lina/data/idf_weight.txt"))) {
//        try (BufferedReader reader = new BufferedReader(new FileReader("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/most_frequent_combinations_query.txt"))) {
//            String line = reader.readLine();
//            while (line != null) {
//                String[] pair = line.split(" ");
//                if (pair.length == 2)
//                    combination.put(pair[0], pair[1]);
//                line = reader.readLine();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return combination;
//    }

    public static void buildMultiMap(LeafReader leafReader) throws IOException{
        TokenStream ts = new TermsEnumTokenStream(leafReader.terms(FIELD).iterator());
        Terms terms = MultiFields.getTerms(leafReader, FIELD);
        TermsEnum termsEnum = terms.iterator();
        // get size of tokenstream as expectedInsertions
//        BloomFilter bloomFilter = BloomFilter.create(memberFunnel, expectedInsertions);
//        multimap = HashMultimap.create();
        multimap = new HashMap<>();
        String str = null;
        while ((str = ((TermsEnumTokenStream) ts).incrementStringToken()) != null) {
//            bloomFilter.put(str);
            BitSet bitSet = multimap.computeIfAbsent(str, s -> new BitSet(Batch_Size));
            termsEnum.seekExact(new BytesRef(str.getBytes()));
            PostingsEnum docs = termsEnum.postings(null, PostingsEnum.NONE);
            int id = docs.nextDoc();
            while (id != DocIdSetIterator.NO_MORE_DOCS) {
//                multimap.put(str, id);
                bitSet.set(id);
                id = docs.nextDoc();
            }
        }
//        System.out.println("multimap size:" + multimap.entries().size());
        ts.close();
//        for (Map.Entry<String, String> entry : commonCombinations.entrySet()) {
//            String key = entry.getKey();
//            String value = entry.getValue();
//            if (multimap.containsKey(key) && multimap.containsKey(value)) {
//                BitSet ids = multimap.get(key).get(0, main.Batch_Size);
//                ids.and(multimap.get(value));
//                multimap.put((key+" "+value), ids);
//            }
//        }
    }


    static void addQueries(Monitor monitor, String queriesFile) throws Exception {
        List<MonitorQuery> queries = new ArrayList<>();
        int count = 0;
        logger.info(String.format(locale,"Loading queries from %s", queriesFile));
        try (FileInputStream fis = new FileInputStream(queriesFile);
             BufferedReader br = new BufferedReader(new InputStreamReader(fis,Charsets.UTF_8))) {
            String queryString;
            while ((queryString = br.readLine()) != null) {
                if (Strings.isNullOrEmpty(queryString))
                    continue;
                String split[] = queryString.split("\t");
                if (split.length > 1) {
                    Map<String, String> metadata = new HashMap<>();
                    metadata.put("duplication", split[1]);
                    queries.add(new MonitorQuery(String.format(locale,"%d", count++), split[0], metadata));
                }
                else {
                    queries.add(new MonitorQuery(String.format(locale,"%d", count++), queryString));
                }

            }
        }
        monitor.update(queries);

        logger.info(String.format(locale,"Added %d queries to monitor", count));
    }

    static List<InputDocument> buildDocs(File filePath) throws Exception {
        List<InputDocument> docs = new ArrayList<>();
        String content;
        try (FileInputStream fis = new FileInputStream(filePath);
             InputStreamReader reader = new InputStreamReader(fis, Charsets.UTF_8)) {
            content = CharStreams.toString(reader);
            InputDocument doc = InputDocument.builder(filePath.toString())
                    .addField(FIELD, content, new StandardAnalyzer())
                    .build();
            docs.add(doc);
        }
        return docs;
    }

    static List<InputDocument> buildDirectoryDocs(File dir) {
        List<InputDocument> docs = new ArrayList<>();
        try {
            //check if the order of iteration is the same every time: Y
            File[] files = dir.listFiles();
//            int batch_start = iteration * Batch_Size;
            int batch_start = last_batch_end;
            int batch_end = batch_start + Batch_Size;
            for (int i = 0; i < files.length ; i++) {
                if (files[i].isDirectory()) {
                    docs.addAll(buildDirectoryDocs(files[i]));
                } else {
                    if (i <= batch_start)
                        i = batch_start;
                    if (i == batch_end) {
                        iteration++;
                        break;
                    }
                    docs.addAll(buildDocs(files[i]));
                }
            }
            last_batch_end = batch_end;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return docs;
    }

    static void outputMatches(Matches<QueryMatch> matches) {
        falsePositives = new HashSet<>();
        falsePositiveCount = 0;
        try(BufferedWriter writer = Files.newBufferedWriter(path, Charset.forName("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
            for (DocumentMatches<QueryMatch> docMatches : matches) {
                if (docMatches.getMatches().size() == 0)
                    continue;
//                logger.log(Level.INFO, "\nMatches from {0}", docMatches.getDocId());
                writer.write(docMatches.getDocId());
                writer.newLine();
                for (QueryMatch match : docMatches) {
//                logger.log(Level.INFO, "\tQuery: {0} ({1} hits)", new Object[]{match.getQueryId(), match.getHitCount()});
//                    logger.log(Level.INFO, "\tQuery: {0} {1}", new Object[]{match.getQueryId(), match.getQuery()});
//                    if (match.getQueryId() == null)
//                        System.out.println("problem");
//                    if (docMatches.getDocId().contains("clueweb09-en0000-00-16101"))
//                        System.out.println("problem");
//                    if (match.getQueryId().equalsIgnoreCase("1055199")) {
//                        logger.log(Level.INFO, "\nMatches from {0}", docMatches.getDocId());
//                    }
//                    logger.log(Level.INFO, "\tQuery: {0}", new Object[]{match.getQueryId()});
                    writer.write(match.getQueryId());
                    writer.newLine();
                    falsePositives.add(match.getQueryId());
                    falsePositiveCount++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/compare_results/org.txt"));

        logger.log(Level.INFO, "Matched batch of {0} documents in {1} milliseconds with {2} queries run",
                new Object[]{matches.getBatchSize(), matches.getSearchTime(), matches.getQueriesRun()});
        totalSearchTime += matches.getSearchTime();
        logger.log(Level.INFO,"Number of distinct false positives {0}, false positives {1}", new Object[]{falsePositives.size(), falsePositiveCount});
    }

}
