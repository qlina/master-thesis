package uk.co.flax.luwak.util;


import java.util.Objects;

public class MadePair<K, V> {

    private K key;
    private V val;

    public MadePair(K key, V val) {
        this.key = key;
        this.val = val;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return val;
    }

    public boolean equals(Object o) {
        if (!(o instanceof MadePair)) {
            return false;
        }
        MadePair<?, ?> p = (MadePair<?, ?>) o;
        return Objects.equals(p.key, key) && Objects.equals(p.val, val);
    }

    public int hashCode() {
        return (key == null ? 0 : key.hashCode()) ^ (val == null ? 0 : val.hashCode());
    }
}
