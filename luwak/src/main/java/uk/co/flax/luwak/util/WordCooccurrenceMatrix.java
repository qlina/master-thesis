package uk.co.flax.luwak.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WordCooccurrenceMatrix {
    public Map<MadePair<Integer, Integer>, Float> matrix;
    public Map<String, Integer> strToInt;
    int wordIndex = 0;
//    private Set<String> words = new HashSet<>();

    public WordCooccurrenceMatrix() {
        readStrIntMapping();
        matrix = new HashMap<>();
//        int linenum = 0;
//        int addLine = 0;
//        try (BufferedReader reader = new BufferedReader(new FileReader("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/100k/word_cooccurrence_3.txt"))) {
        try (BufferedReader reader = new BufferedReader(new FileReader("/home/lina/data/pruned_cooccur_matrix_int.txt"))) {
            String line = reader.readLine();
            while (line != null) {
//                linenum++;
                String[] pair = line.split("\\s+");
                if (pair.length == 3) {
//                    MadePair tmp = new MadePair<>(strToInt.get(pair[0]), strToInt.get(pair[1]));
//                    if (matrix.containsKey(tmp)) {
//                        System.out.println(pair[0]+ " "+ pair[1] + " " + matrix.get(tmp));
//                        System.out.println(Float.valueOf(pair[2]));
//                    }
                    matrix.put(new MadePair<>(Integer.valueOf(pair[0]), Integer.valueOf(pair[1])), Float.valueOf(pair[2]));
//                    addLine ++;
                }
//                else {
//                    System.out.println(line);
//                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("matrix size "+matrix.size());
//        System.out.println("linenum " + linenum);
//        System.out.println("add line " + addLine);
//        String emptyString = "";
//        System.out.println("matrix used memory: " + main.bytesToMegabytes(InstrumentationAgent.getObjectSize(matrix)));
//        System.out.println("mapping used memory: " + main.bytesToMegabytes(InstrumentationAgent.getObjectSize(strToInt)));
//        System.out.println("empty string used memory: " + InstrumentationAgent.getObjectSize(emptyString));
//        Runtime runtime = Runtime.getRuntime();
//        runtime.gc();
//        main.memoryUsed = runtime.totalMemory() - runtime.freeMemory();

    }

    public void readStrIntMapping() {
        strToInt = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("/home/lina/data/str_int_map.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                String[] mapping = line.split("\\s+");
                if (mapping.length == 2) {
                    strToInt.put(mapping[0], Integer.valueOf(mapping[1]));
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("mapping size "+strToInt.size());
    }

    public float findProb(MadePair<String, String> key) {
        float prob = 0;
        MadePair<Integer, Integer> keyInt = new MadePair<>(strToInt.get(key.getKey()), strToInt.get(key.getValue()));
        if (matrix.containsKey(keyInt)) {
            prob = matrix.get(keyInt);
//            System.out.println(key.getKey() + " " + key.getValue() + " found");
        }
        else {
            MadePair reverseKey = new MadePair(keyInt.getValue(), keyInt.getKey());
            if (matrix.containsKey(reverseKey)) {
                prob = matrix.get(reverseKey);
//                System.out.println(reverseKey.getKey() + " " + reverseKey.getValue() + " found");
            }
//            else {
//                System.out.println(reverseKey.getKey() + " " + reverseKey.getValue() + " not found");
//            }
        }
        return prob;
    }


}

//public class WordCooccurrenceMatrix {
//    public Map<MadePair, Double> WordCooccurrenceMap;
//
//    public WordCooccurrenceMatrix() {
//        this.WordCooccurrenceMap = new HashMap<>();
////        try (BufferedReader reader = new BufferedReader(new FileReader("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/100k/word_cooccurrence_3.txt"))) {
//        try (BufferedReader reader = new BufferedReader(new FileReader("/home/lina/data/word_cooccurrence_3.txt"))) {
//            String line = reader.readLine();
//            while (line != null) {
//                String[] pair = line.split("\\s+");
//                if (pair.length == 3)
//                    this.WordCooccurrenceMap.put(new MadePair<>(pair[0], pair[1]), Double.valueOf(pair[2]));
//                line = reader.readLine();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("matrix size "+ WordCooccurrenceMap.size());
//    }
//
//
//    public double findProb(MadePair<String, String> key) {
//        double prob = 0.0;
//        if (this.WordCooccurrenceMap.containsKey(key)) {
//            prob = this.WordCooccurrenceMap.get(key);
////            System.out.println(key.getKey() + " " + key.getValue() + " found");
//        }
//        else {
//            MadePair reverseKey = new MadePair(key.getValue(), key.getKey());
//            if (this.WordCooccurrenceMap.containsKey(reverseKey)) {
//                prob = this.WordCooccurrenceMap.get(reverseKey);
////                System.out.println(reverseKey.getKey() + " " + reverseKey.getValue() + " found");
//            }
////            else {
////                System.out.println(reverseKey.getKey() + " " + reverseKey.getValue() + " not found");
////            }
//        }
//        return prob;
//    }
//
//
//}
