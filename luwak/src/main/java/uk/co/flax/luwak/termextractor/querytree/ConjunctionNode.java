package uk.co.flax.luwak.termextractor.querytree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.lucene.index.Term;
import org.apache.lucene.util.PriorityQueue;
import uk.co.flax.luwak.main;
import uk.co.flax.luwak.termextractor.QueryTerm;
import uk.co.flax.luwak.util.MadePair;

/*
 * Copyright (c) 2014 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class ConjunctionNode extends QueryTree {

    private static final Comparator<QueryTree> COMPARATOR = Comparator.comparingDouble(QueryTree::weight).reversed();

    private final List<QueryTree> children = new ArrayList<>();

    private int duplications;
    private double[] t_check = {7745, 10328, 12645, 14520, 17457};// should use no avoid search, no expected doc >= 1
    private double[] t_duplication = {1126, 804, 695, 624, 629};// 600, last: >5
    private double[] t_intersection = {101, 95, 98, 102, 97};// 600
//    private double[] t_and_overhead = {536, 625, 653, 653, 778};// 600
    private double[] t_avoid = {4067, 2066, 1414, 1141, 1098};// 600
//    private double[] t_check = {7381, 9574, 11656, 13093, 16882};// should use no avoid search, no expected doc >= 1
//    private double[] t_duplication = {868, 689, 576, 500, 561};// 400, last: >7
//    private double[] t_intersection = {94, 85, 90, 98, 92};// 400
////    private double[] t_and_overhead = {560, 675, 710, 870};// 400
//    private double[] t_avoid = {3282, 1789, 1343, 1048, 1046};// 400
//    private double[] t_check = {9040, 11136, 13690, 15521, 18719};// no avoid search, out-of-date
//    private double[] t_duplication = {1607, 1044, 852, 661, 710};// 1000
//    private double[] t_intersection = {113, 105, 104, 109, 105};// 1000
//    private double[] t_avoid = {6786, 3168, 2030, 1392, 1490};// 1000
//    private double[] t_check = {6870, 8560, 10695, 12535, 16446};// 200
//    private double[] t_duplication = {585, 509, 469, 478, 530};// 200
//    private double[] t_intersection = {85, 76, 81, 95, 92};// 200
////    private double[] t_and_overhead = {526, 611, 838, 799};// 300
//    private double[] t_avoid = {2268, 1215, 968, 803, 885};// 200
    private double total_duplications;
    private double time_running_q;
    private int avgDocSize = 843;

    private int para;

    private ConjunctionNode(List<QueryTree> children) {
        this.children.addAll(children);
        this.children.sort(COMPARATOR);
    }

    public static QueryTree build(List<QueryTree> children) {
        if (children.size() == 0)
            throw new IllegalArgumentException("Cannot build ConjunctionNode with no children");
        if (children.size() == 1)
            return children.get(0);
        List<QueryTree> restrictedChildren = children.stream().filter(c -> c.isAny() == false).collect(Collectors.toList());
        if (restrictedChildren.size() == 0) {
            // all children are ANY nodes, so return the first one
            return children.get(0);
        }
        if (restrictedChildren.size() == 1) {
            return restrictedChildren.get(0);
        }
        return new ConjunctionNode(restrictedChildren);
    }

    public static QueryTree build(QueryTree... children) {
        return build(Arrays.asList(children));
    }

    @Override
    public double weight() {
        return children.get(0).weight();
    }

    @Override
    public void collectTerms(List<QueryTerm> termsList) {
//        int n_index = (int)Math.ceil(this.children.size() / 2.0);
//        int n_index = para < 0 ? 0 : decideNumExtra();
//        int n_index = 0;
//        int n_index = decideNumExtra();
//        main.numExtra += n_index;
        main.numExtra += children.size();
//        for (int i = 0; i <= n_index; i++) {
        for (int i = 0; i < children.size(); i++) {
            children.get(i).collectTerms(termsList);
        }
    }

//    @Override
//    public void collectTerms(Set<QueryTerm> termsList) {
//        children.get(0).collectTerms(termsList);
//    }
    @Override
    public void setDuplications(int duplications) {
        this.duplications = duplications - 1;
        this.para = children.size() > 5 ? 4 : (children.size()-2);
        this.total_duplications = this.duplications * t_duplication[para];
        this.time_running_q = this.total_duplications + t_check[para];
//        this.time_running_q = t_check[para];
    }
    // why all terms indexed?
    public int decideNumExtra() {
        int maximum = children.size();
//        return maximum;
        // queries would be filtered out by the fst term
//        if (children.get(0).weight() == 100) {
//            main.numOOVs += 1;
//            System.out.println(children);
            // this works, maybe can be applied to queries that very likely would be filtered by PQ
//            return 0;
//        }

////////////////////////////////////////////////language model version
//        double preProb = Math.pow(10, -(children.get(0).weight()) / Math.log(10.0D));
//        for (; extra_index < maximum; extra_index++) {
//            double currentProb = Math.pow(10, -(children.get(extra_index).weight()) / Math.log(10.0D));
//            if (extra_index == 1) {
//                preProb = (1-Math.pow((1-preProb), avgDocSize))*Math.pow((1-currentProb),avgDocSize) + Math.pow((1-preProb), avgDocSize);
//            } else {
//                preProb = calculateProbNoOccurence(currentProb, (1-preProb));
//            }
//            if (crossThreshold(preProb, extra_index))
//                break;
//        }

////////////////////////////////////////////////normal routine
//        int range = 0;
//        int extra_index = 1;
//        double preProb = 1;
////        double preProb = 1 / Math.exp(children.get(0).weight() - 1);
//        for (; extra_index < maximum; extra_index++) {
//            double currentProb = 1 / Math.exp(children.get(extra_index).weight() - 1);
//            preProb = preProb * currentProb;
//            if (crossThreshold((1-preProb), extra_index)) {
////                System.out.println("cross threshold");
//                main.xth += 1;
//                range = extra_index;
//                break;
//            }
//        }
//        if (range == 0)
//            range = extra_index-1;
//        return range;

////////////////////////////////////////////////latest routine
        double pCooccurrence = 1;
        double max_gain = 0;
        int max_gain_index = 0;
        int no_doc_index = maximum-1;
        double p_no_doc = 0;
        int num_very_frequent = 0;
        try {
            ArrayList<Double> probs = new ArrayList<>();
            for (int index = 1; index < maximum; index++) {
                String fstWord = ((TermNode) children.get(index-1)).getTerm().term.text();
                String othWord = ((TermNode) children.get(index)).getTerm().term.text();
                MadePair key = new MadePair(othWord, fstWord);
//                double searchedProb = main.wordMatrix.findProb(key);
                double searchedProb = 0;
                if (searchedProb > 0) {
                    pCooccurrence *= searchedProb;
                    num_very_frequent += 1;
//                    System.out.println(">0");
                }
                else {
//                    double single_prob = Math.pow(10, -(children.get(index).weight()) / Math.log(10.0D));
//                    double weight = (1-Math.pow((1-single_prob), avgDocSize));
                    double weight = 1 / Math.exp(children.get(index).weight() - 1);
                    main.numSingles += 1;
                    pCooccurrence *= weight;
                    if (weight > 0.013){ //top 0.66% in sorted weights, 59%
                        num_very_frequent += 1;
                        main.num2 += 1;
                    }
                }
                probs.add(pCooccurrence);
                if (main.Batch_Size * pCooccurrence < 1 && index < no_doc_index) {
                    no_doc_index = index;
                    p_no_doc = pCooccurrence;

                }

            }

            for (int index = no_doc_index; index < maximum; index++) {
                if (index != maximum-1) {
                    double current_gain = time_running_q * (1-p_no_doc) - index * t_intersection[para];
                    if (current_gain >= max_gain) {
                        max_gain = current_gain;
                        max_gain_index = index;
                    }
                }
                else {
                    double current_gain = time_running_q - index * t_intersection[para] - p_no_doc * t_avoid[para];
                    if (current_gain >= max_gain) {
                        max_gain = current_gain;
                        max_gain_index = index;
                    }
                }
                p_no_doc = probs.get(index);
            }

        } catch (Exception e){
            pCooccurrence = 0;
        }
        if (num_very_frequent >= (maximum-1)) {
            return maximum-1;
        }
        else {
            return  max_gain_index;
        }


//        if (main.Batch_Size * pCooccurrence >= 1) {
//        return (maximum-1);
//        System.out.println(max_gain_index + " " + children.size());
////////////////////////////////////////////////predict if a query would be filtered or not
//        if (main.Batch_Size*preProb >= 1) {
//            return 0;
//        }

////////////////////////////////////////////////maximization
//        preProb = 1;
////        preProb = 1 / Math.exp(children.get(0).weight() - 1);
////        int range = extra_index - 1;
//        if (range > 0) {
//            double max_gain = 0;
//            int extra = 0;
//            for (int i = 1; i <= range; i++) {
//                double currentProb = 1 / Math.exp(children.get(i).weight() - 1);
//                preProb = preProb * currentProb;
//                double current_gain = time_running_q * (1-preProb) - i * t_intersection[para];
//                if (current_gain >= max_gain) {
//                    max_gain = current_gain;
//                    extra = i;
//                }
//
//            }
//            return extra;
//        }
//        else {
//            return 0;
//        }
    }

//    double calculateProbNoOccurence(double currentProb, double preCooccurProb) {
////        double prob = Math.pow((1-preCooccurProb), avgDocSize) + Math.pow((1-currentProb), avgDocSize) - Math.pow((1-preCooccurProb-currentProb), avgDocSize);
//        double prob = (1-preCooccurProb) + Math.pow((1-currentProb), avgDocSize) - (1-preCooccurProb)*Math.pow((1-currentProb), avgDocSize);
//        return prob;
//    }

//    public boolean crossThreshold(double probNoCooccurence, int extra_index) {
////        if (extra_index * t_intersection >= time_running_q * Math.pow((1 - cumulative_prob), 300))
////        if (extra_index * t_intersection[para] >= time_running_q * Math.pow(probNoCooccurence, main.Batch_Size))
//        double cost = extra_index * t_intersection[para] + t_and_overhead[para];
////        if (extra_index == maximum - 1) {
////            if (cost + time_running_q * (1-probNoCooccurence) >= time_running_q * probNoCooccurence)
////                return true;
////        }
////        else {
////            if (cost >= time_running_q * probNoCooccurence)
////                return true;
////        }
//        if (cost >= time_running_q * probNoCooccurence)
//                return true;
//        return false;
//    }

    @Override
    public boolean advancePhase(float minWeight) {
        if (children.get(0).advancePhase(minWeight)) {
            this.children.sort(COMPARATOR);
            return true;
        }
        if (children.size() == 1) {
            return false;
        }
        if (children.get(1).weight() <= minWeight) {
            return false;
        }
        children.remove(0);
        return true;
    }

    @Override
    public void visit(QueryTreeVisitor visitor, int depth) {
        visitor.visit(this, depth);
        for (QueryTree child : children) {
            child.visit(visitor, depth + 1);
        }
    }

    @Override
    public boolean isAny() {
        for (QueryTree child : children) {
            if (!child.isAny())
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Conjunction[" + children.size() + "]^" + weight() + " " + children.get(0).toString();
    }

}
