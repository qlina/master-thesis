//package uk.co.flax.luwak.termextractor.weights;
//
//import java.util.Map;
//
//import edu.stanford.nlp.mt.lm.ARPALMState;
//import edu.stanford.nlp.mt.lm.ARPALanguageModel;
//import edu.stanford.nlp.mt.util.ArraySequence;
//import edu.stanford.nlp.mt.util.IString;
//import edu.stanford.nlp.mt.util.Sequence;
//import uk.co.flax.luwak.termextractor.QueryTerm;
///*
// * Copyright (c) 2014 Lemur Consulting Ltd.
// * <p/>
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// * <p/>
// * http://www.apache.org/licenses/LICENSE-2.0
// * <p/>
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
///**
// * Weights more infrequent terms more highly
// */
//public class TermLMWeightNorm extends WeightNorm {
//
//    private final ARPALanguageModel languageModel;
//
//    /**
//     * Creates a TermFrequencyNorm
//     * @param languageModel built from training set of documents
//     */
//    public TermLMWeightNorm(ARPALanguageModel languageModel) {
//        this.languageModel = languageModel;
//    }
//
//    @Override
//    public float norm(QueryTerm term) {
//        IString target = new IString(term.term.text());
//        Sequence<IString> sequence = new ArraySequence<IString>(new IString[]{target});
//        return -(float)languageModel.scoreNgram(sequence).getScore();
//    }
////(float)Math.log(10.0D)
//}
