package uk.co.flax.luwak.presearcher;

/*
 * Copyright (c) 2013 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.TermToBytesRefAttribute;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queries.TermsQuery;
import org.apache.lucene.search.*;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.BytesRefHash;
import org.apache.lucene.util.BytesRefIterator;
import uk.co.flax.luwak.*;
//import uk.co.flax.luwak.analysis.BloomFilterTokenFilter;
import uk.co.flax.luwak.analysis.BytesRefFilteredTokenFilter;
import uk.co.flax.luwak.analysis.TermsEnumTokenStream;
import uk.co.flax.luwak.termextractor.QueryAnalyzer;
import uk.co.flax.luwak.termextractor.QueryTerm;
import uk.co.flax.luwak.termextractor.querytree.QueryTree;
import uk.co.flax.luwak.termextractor.querytree.QueryTreeViewer;
import uk.co.flax.luwak.termextractor.weights.TermIDFWeightNorm;
//import uk.co.flax.luwak.termextractor.weights.TermLMWeightNorm;
import uk.co.flax.luwak.termextractor.weights.TermWeightor;

//import edu.stanford.nlp.mt.lm.ARPALanguageModel;
import uk.co.flax.luwak.termextractor.weights.TokenLengthNorm;
import uk.co.flax.luwak.util.MadePair;

//import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Presearcher implementation that uses terms extracted from queries to index
 * them in the Monitor, and builds a BooleanQuery from InputDocuments to match
 * them.
 *
 * This Presearcher uses a QueryTermExtractor to extract terms from queries.
 */
public class TermFilteredPresearcher extends Presearcher {

    /**
     * The default TermWeightor, weighting by token length
     */
    public static Map<String, Float> queryTermFreq = readQueryTermFreq();
    //    public static final TermWeightor DEFAULT_WEIGHTOR = new TermWeightor(new TokenLengthNorm(), new TermFrequencyWeightNorm(queryTermFreq, 3, 1));

//    public static ARPALanguageModel lm = readARPALanguageModel();
    public static final TermWeightor DEFAULT_WEIGHTOR = new TermWeightor(new TermIDFWeightNorm(queryTermFreq));
//    public static final TermWeightor DEFAULT_WEIGHTOR = new TermWeightor(new TermLMWeightNorm(lm));

    static {
        BooleanQuery.setMaxClauseCount(Integer.MAX_VALUE);
    }

    protected final QueryAnalyzer extractor;
    protected final TermWeightor weightor;
    protected List<LeafReader> leafReaders = new LinkedList<>();
    protected static ArrayList<String> delDocs = new ArrayList<>();
//    public int nCollectedTerms = 0;
    private final List<PresearcherComponent> components = new ArrayList<>();

    public static final String ANYTOKEN_FIELD = "__anytokenfield";
    public static final String ANYTOKEN = "__ANYTOKEN__";
    public static final String FIELD = "text";

    /**
     * Create a new TermFilteredPresearcher using a defined TermWeightor
     *
     * @param weightor   the TermWeightor
     * @param components optional PresearcherComponents
     */
    public TermFilteredPresearcher(TermWeightor weightor, PresearcherComponent... components) {
        this.extractor = QueryAnalyzer.fromComponents(components);
        this.components.addAll(Arrays.asList(components));
        this.weightor = weightor;
    }

    /**
     * Create a new TermFilteredPresearcher using the default term weighting
     *
     * @param components optional PresearcherComponents
     */
    public TermFilteredPresearcher(PresearcherComponent... components) {
        this(DEFAULT_WEIGHTOR, components);
    }

    public static Map<String, Float> readQueryTermFreq() {
        Map<String, Float> freq = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("/home/lina/data/idf_weight_single_token.txt"))) {
//        try (BufferedReader reader = new BufferedReader(new FileReader("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/100k/idf_weight.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                String[] pair = line.split("\t");
                if (pair.length == 2)
                    freq.put(pair[0], Float.valueOf(pair[1]));
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return freq;
    }

//    public static ARPALanguageModel readARPALanguageModel(){
//        try {
//            return new ARPALanguageModel("/Users/LinaQiu/Qlina/TrecExpe/src/main/resources/100k/cluewebmodel.lm");
////            return new ARPALanguageModel("/home/lina/LanguageModel/cluewebmodel.lm");
//        } catch (IOException e) {
//            return null;
//        }
//
//    }

    @Override
    public final Query buildQuery(LeafReader reader, QueryTermFilter queryTermFilter) {
        try {
            DocumentQueryBuilder queryBuilder = getQueryBuilder();
            for (String field : reader.fields()) {
                if (field.equalsIgnoreCase(FIELD)) {
                    TokenStream ts = new TermsEnumTokenStream(reader.terms(field).iterator());
                    for (PresearcherComponent component : components) {
                        ts = component.filterDocumentTokens(field, ts);
                    }
                    ts = new BytesRefFilteredTokenFilter(ts, queryTermFilter.getTerms(field));
                    TermToBytesRefAttribute termAtt = ts.addAttribute(TermToBytesRefAttribute.class);
                    while (ts.incrementToken()) {
                        queryBuilder.addTerm(field, BytesRef.deepCopyOf(termAtt.getBytesRef()));
                    }
                    ts.close();
                }

            }
//            long startTime = System.nanoTime();
            Query presearcherQuery = queryBuilder.build();
            BooleanQuery.Builder bq = new BooleanQuery.Builder();
            bq.add(presearcherQuery, BooleanClause.Occur.SHOULD);
            bq.add(new TermQuery(new Term(ANYTOKEN_FIELD, ANYTOKEN)), BooleanClause.Occur.SHOULD);
            presearcherQuery = bq.build();

            for (PresearcherComponent component : components) {
                presearcherQuery = component.adjustPresearcherQuery(reader, presearcherQuery);
            }
            return presearcherQuery;
        }
        catch (IOException e) {
            // We're a MemoryIndex, so this shouldn't happen...
            throw new RuntimeException(e);
        }
    }

    protected DocumentQueryBuilder getQueryBuilder() {
        return new DocumentQueryBuilder() {

            List<Term> terms = new ArrayList<>();

            @Override
            public void addTerm(String field, BytesRef term) throws IOException {
                terms.add(new Term(field, term));
            }

            @Override
            public Query build() {
                return new TermsQuery(terms);
            }

            @Override
            public int getTermsSize() {return terms.size();}
        };
    }

    public static Set<String> getDelDocs() {
        return new HashSet<>(delDocs);
    }

    public static final FieldType QUERYFIELDTYPE;
    public static final FieldType QUERYEXTRATYPE;

    static {
        QUERYFIELDTYPE = new FieldType(TextField.TYPE_NOT_STORED);
        QUERYFIELDTYPE.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        QUERYFIELDTYPE.freeze();
    }

    static {
        QUERYEXTRATYPE = new FieldType(StoredField.TYPE);
        QUERYEXTRATYPE.setIndexOptions(IndexOptions.NONE);
        QUERYEXTRATYPE.freeze();
    }


    @Override
    public final Document indexQuery(Query query, Map<String, String> metadata) {

        QueryTree querytree = extractor.buildTree(query, weightor);
        querytree.setDuplications(Integer.valueOf(metadata.get("duplication")));
        Document doc = buildQueryDocument(querytree);

        for (PresearcherComponent component : components) {
            component.adjustQueryDocument(doc, metadata);
        }

        return doc;
    }

    /**
     * Debugging: write the parsed query tree to a PrintStream
     *
     * @param query the query to analyze
     * @param out   a {@link PrintStream}
     */
    public void showQueryTree(Query query, PrintStream out) {
        QueryTreeViewer.view(extractor.buildTree(query, weightor), out);
    }

    protected Document buildQueryDocument(QueryTree querytree) {
        MadePair<Map<String, BytesRefHash>, List<BytesRef>> list  = collectTerms(querytree);
        // check size of list
        Map<String, BytesRefHash> fieldTerms = list.getKey();
        List<BytesRef> extraFieldTerms = list.getValue();
        Document doc = new Document();
        for (Map.Entry<String, BytesRefHash> entry : fieldTerms.entrySet()) {
            doc.add(new Field(entry.getKey(),
                    new TermsEnumTokenStream(new BytesRefHashIterator(entry.getValue())), QUERYFIELDTYPE));
        }
        BytesRef extra = ExtraTerms.serialize(extraFieldTerms);
//        for (BytesRef entry : extraFieldTerms) {
//            doc.add(new Field(Monitor.FIELDS.extra, entry, QUERYEXTRATYPE));
//        }
        doc.add(new BinaryDocValuesField(Monitor.FIELDS.extra, extra));
        doc.add(new BinaryDocValuesField(Monitor.FIELDS.num, new BytesRef(String.valueOf(extraFieldTerms.size()))));

        return doc;
    }

    protected class BytesRefHashIterator implements BytesRefIterator {

        final BytesRef scratch = new BytesRef();
        final BytesRefHash terms;
        final int[] sortedTerms;
        int upto = -1;


        public BytesRefHashIterator(BytesRefHash terms) {
            this.terms = terms;
            this.sortedTerms = terms.sort();
        }

        @Override
        public BytesRef next() throws IOException {
            if (upto >= sortedTerms.length)
                return null;
            upto++;
            if (sortedTerms[upto] == -1)
                return null;
            this.terms.get(sortedTerms[upto], scratch);
            return scratch;
        }
    }

    // One round is enough
    protected MadePair<Map<String, BytesRefHash>, List<BytesRef>> collectTerms(QueryTree tree) {

        Map<String, BytesRefHash> fieldTerms = new HashMap<>();
        List<BytesRef> extraFieldTerms = new ArrayList<>();
        List<QueryTerm> terms = extractor.collectTerms(tree);
        for (int i = 0; i < terms.size(); i++) {
            QueryTerm queryTerm = terms.get(i);
            if (i == 0) {
                if (queryTerm.type.equals(QueryTerm.Type.ANY)) {
                    fieldTerms.computeIfAbsent(ANYTOKEN_FIELD, f -> {
                        BytesRefHash hash = new BytesRefHash();
                        hash.add(new BytesRef(ANYTOKEN));
                        return hash;
                    });
                } else {
                    BytesRefHash termslist = fieldTerms.computeIfAbsent(queryTerm.term.field(), f -> new BytesRefHash());
                    termslist.add(queryTerm.term.bytes());
                    if (!queryTerm.type.equals(QueryTerm.Type.EXACT)) {
                        for (PresearcherComponent component : components) {
                            BytesRef extratoken = component.extraToken(queryTerm);
                            if (extratoken != null)
                                termslist.add(extratoken);
                        }
                    }
                }
            }
            extraFieldTerms.add(queryTerm.term.bytes());



        }
        return new MadePair(fieldTerms, extraFieldTerms);

    }
}
